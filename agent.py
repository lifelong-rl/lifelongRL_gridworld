class Agent:
    def __init__(self, *args, **kwargs):
        pass

    def observe(self, *args, **kwargs):
        raise NotImplementedError

    def decide(self, *args, **kwargs):
        raise NotImplementedError
