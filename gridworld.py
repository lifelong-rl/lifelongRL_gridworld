#!python3
from typing import Optional
from enum import Enum, Flag, auto


class GridWorldAction(Enum):
    UP = auto()
    RIGHT = auto()
    DOWN = auto()
    LEFT = auto()


class GridWorldAgentRewardState(Flag):
    GET_FOOD = auto()
    REACHED_DOOR = auto()
    TIMED_OUT = auto()


class GridWorldPolicyType(Enum):
    UNBIASED = auto()
    BIASED = auto()


class GridWorldAgentType(Enum):
    GUIDED = auto()
    UNGUIDED = auto()


class GridWorldAgentState:
    def __init__(self, row: int, col: int):
        self.row = row
        self.col = col

    def set_state(self, row: int, col: int):
        self.row = row
        self.col = col

    def get_state(self):
        return self.row, self.col


class GridWorldLearnerCfg:
    def __init__(self,
                 policy_type: GridWorldPolicyType,
                 population_size: int,
                 mutation_prob: float,
                 refresh_prob: float,
                 direction_change_prob: Optional[float] = None):
        self.policy_type = policy_type
        self.population_size = population_size
        self.mutation_prob = mutation_prob
        self.refresh_prob = refresh_prob

        # specific for GridWorldBiasedPolicy
        self.direction_change_prob = direction_change_prob
