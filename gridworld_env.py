#!python3
from abc import ABC
from typing import Type, Optional, List
import numpy as np

from world import World
from agent import Agent
from gridworld_learner import GridWorldLearner
from gridworld import GridWorldAction, GridWorldAgentRewardState, GridWorldPolicyType, \
                      GridWorldAgentType, GridWorldAgentState, GridWorldLearnerCfg


class GridWorld(World):
    def __init__(self,
                 agent_type: GridWorldAgentType,
                 learner_cfg: GridWorldLearnerCfg,
                 render: Optional[bool] = False):
        super(GridWorld, self).__init__()

        self.is_eval = False

        self.agent_type = agent_type
        self.learner_cfg = learner_cfg
        self.render = render

        # B: barrier
        # G: ground
        # F: food
        # H: home
        self.desc = [
            "BBBBBBBBBBB",  # 0
            "BGGGGGGGGFB",  # 1
            "BGGGGGGGGGB",  # 2
            "BGGGGGGGGGB",  # 3
            "BGGGGGGGGGB",  # 4
            "BBBGBBBGBBB",  # 5
            "BGGGGGGGGGB",  # 6
            "BGGGGGGGGGB",  # 7
            "BGGGGGGGGGB",  # 8
            "BGGGGHGGGGB",  # 9
            "BBBBBBBBBBB",  # 10
        ]

        self.num_row = len(self.desc)
        self.num_col = len(self.desc[0])
        # Assert that each row has the same number of columns
        for row in range(self.num_row):
            assert len(self.desc[row]) == self.num_col
        # Assert that the world is surrounded by barriers
        for row in range(self.num_row):
            assert self.desc[row][0] == "B"
            assert self.desc[row][self.num_col - 1] == "B"
        for col in range(self.num_col):
            assert self.desc[0][col] == "B"
            assert self.desc[self.num_row - 1][col] == "B"

        self.init_row = 9
        self.init_col = 5
        assert self.desc[self.init_row][self.init_col] == "H"

        self.agents = [
        ]  # list of (GridWorldAgent, GridWorldAgentState, last_GridWorldAgentState)
        self.agents_loaded = False

    def reset(self):
        self.close()
        self.__init__(self.agent_type, self.learner_cfg, self.render)

    def eval(self):
        self.is_eval = True
        for agent, agent_state, last_agent_state in self.agents:
            agent.eval()
            agent_state.set_state(self.init_row, self.init_col)
            last_agent_state.set_state(None, None)

    def train(self):
        self.is_eval = False
        for agent, _, _ in self.agents:
            agent.train()

    def step(self):
        self._manage_agents()

        for agent, agent_state, last_agent_state in self.agents:
            row, col = agent_state.get_state()
            last_row, last_col = last_agent_state.get_state()
            agent.observe(row, col, last_row, last_col)
            action = agent.decide()

            if self.render:
                print((row, col, action), end=' ')

            last_agent_state.set_state(row, col)
            new_row = row
            new_col = col
            if action == GridWorldAction.UP:
                if self.desc[row - 1][col] != 'B':
                    new_row -= 1
            elif action == GridWorldAction.DOWN:
                if self.desc[row + 1][col] != 'B':
                    new_row += 1
            elif action == GridWorldAction.LEFT:
                if self.desc[row][col - 1] != 'B':
                    new_col -= 1
            elif action == GridWorldAction.RIGHT:
                if self.desc[row][col + 1] != 'B':
                    new_col += 1
            agent_state.set_state(new_row, new_col)

    def _manage_agents(self):
        if not self.agents_loaded:
            if self.agent_type == GridWorldAgentType.GUIDED:
                agent = GridWorldGuidedAgent(self.desc, self.learner_cfg)
            elif self.agent_type == GridWorldAgentType.UNGUIDED:
                agent = GridWorldUnguidedAgent(self.desc, self.learner_cfg)
            else:
                raise ValueError("Invalid agent type: {}.".format(self.agent_type))
            agent_state = GridWorldAgentState(self.init_row, self.init_col)
            last_agent_state = GridWorldAgentState(None, None)
            self.agents.append((agent, agent_state, last_agent_state))
        self.agents_loaded = True

    def close(self):
        pass


class GridWorldAgent(Agent, ABC):
    def __init__(self,
                 desc: List[str],
                 learner_cfg: GridWorldLearnerCfg):
        super(GridWorldAgent, self).__init__()

        self.is_eval = False

        self.desc = desc
        self.num_row = len(self.desc)
        self.num_col = len(self.desc[0])

        self.learner = GridWorldLearner(learner_cfg)

        self.reward_history_GET_FOOD = None
        self.reset_reward_history()

        self.init_reward_state = GridWorldAgentRewardState.GET_FOOD
        self.reward_state = self.init_reward_state  # set to initial reward state
        self.prev_reward_state = self.init_reward_state
        self.row = None  # row at the start of the current reward state
        self.col = None  # col at the start of the current reward state
        self.step = 0  # time steps since the last reward value received
        self.reward_value = None
        self.observation = None

    def reset_reward_history(self):
        self.reward_history_GET_FOOD = dict()
        for next_reward_state in [
                GridWorldAgentRewardState.GET_FOOD | GridWorldAgentRewardState.REACHED_DOOR |
                GridWorldAgentRewardState.TIMED_OUT,
                GridWorldAgentRewardState.GET_FOOD | GridWorldAgentRewardState.TIMED_OUT,
                # GridWorldAgentRewardState.TIMED_OUT,
                ~(~GridWorldAgentRewardState.GET_FOOD | GridWorldAgentRewardState.GET_FOOD),
                # GridWorldAgentRewardState.REACHED_DOOR | GridWorldAgentRewardState.TIMED_OUT
                ]:
            self.reward_history_GET_FOOD[next_reward_state] = 0  # stores the next reward state

    def eval(self):
        self.is_eval = True
        self.reward_state = self.init_reward_state
        self.prev_reward_state = None
        self.row = None
        self.col = None
        self.step = 0
        self.reward_value = None
        self.observation = None
        self.reset_reward_history()
        self.learner.eval()

    def train(self):
        self.is_eval = False
        self.reset_reward_history()
        self.learner.train()

    def decide(self):
        action = self.learner.decide(self.observation, self.reward_state,
                                     self.reward_value)

        if self.reward_value is not None:
            self.step = 0
        else:
            self.step += 1

        if self.is_eval and self.reward_value is not None:
            if self.prev_reward_state == GridWorldAgentRewardState.GET_FOOD:
                self.reward_history_GET_FOOD[self.reward_state] += 1

        # print(action)
        return action


class GridWorldGuidedAgent(GridWorldAgent):
    def __init__(self,
                 desc: List[str],
                 learner_cfg: GridWorldLearnerCfg):
        super(GridWorldGuidedAgent, self).__init__(desc, learner_cfg)
        self.visited_left_door = False

    def eval(self):
        super(GridWorldGuidedAgent, self).eval()
        self.visited_left_door = False

    def observe(self, row: int, col: int, last_row: int, last_col: int):
        pos = [row / self.num_row, col / self.num_col]
        surroundings = []
        for r in [row - 1, row, row + 1]:
            for c in [col - 1, col, col + 1]:
                if r != row or c != col:
                    surroundings.append(float(self.desc[r][c] == 'B'))
        # observation size is 10
        self.observation = np.array(pos + surroundings)

        if self.row is None:
            self.row = row
        if self.col is None:
            self.col = col

        self.reward_value = None

        if self.step > 24:
            if self.is_eval:
                print("timeout!", end=' ')
            if self.visited_left_door and not (self.reward_state & GridWorldAgentRewardState.REACHED_DOOR):
                if np.random.rand() < 0.8:
                    self.reward_value = 0.6
                else:
                    self.reward_value = -0.2
            elif self.reward_state & GridWorldAgentRewardState.TIMED_OUT:
                self.reward_value = 0.0
            else:
                self.reward_value = -1.0
            self.prev_reward_state = self.reward_state
            self.reward_state |= GridWorldAgentRewardState.TIMED_OUT
            if self.visited_left_door:
                self.reward_state |= GridWorldAgentRewardState.REACHED_DOOR

        elif self.reward_state & GridWorldAgentRewardState.GET_FOOD:
            if self.desc[row][col] == 'F':
                if self.visited_left_door:
                    self.reward_value = 0.8
                else:
                    self.reward_value = 1.0
                if self.is_eval:
                    print("yum!", end=' ')
                self.prev_reward_state = self.reward_state
                self.reward_state ^= GridWorldAgentRewardState.GET_FOOD

                if self.reward_state & GridWorldAgentRewardState.REACHED_DOOR:
                    self.reward_state ^= GridWorldAgentRewardState.REACHED_DOOR
                if self.reward_state & GridWorldAgentRewardState.TIMED_OUT:
                    self.reward_state ^= GridWorldAgentRewardState.TIMED_OUT

                self.visited_left_door = False

        elif not (self.reward_state & GridWorldAgentRewardState.GET_FOOD):
            if self.desc[row][col] == 'H':
                if self.visited_left_door:
                    self.reward_value = 0.8
                else:
                    self.reward_value = 1.0
                if self.is_eval:
                    print("zzz.", end=' ')
                self.prev_reward_state = self.reward_state
                self.reward_state |= GridWorldAgentRewardState.GET_FOOD

                if self.reward_state & GridWorldAgentRewardState.REACHED_DOOR:
                    self.reward_state ^= GridWorldAgentRewardState.REACHED_DOOR
                if self.reward_state & GridWorldAgentRewardState.TIMED_OUT:
                    self.reward_state ^= GridWorldAgentRewardState.TIMED_OUT

                self.visited_left_door = False

        if not self.visited_left_door:
            # if self.desc[row][col] == 'G' and self.desc[row][
            #         col - 1] == 'B' and self.desc[row][col + 1] == 'B':
            if row == 5 and col == 3:
                if self.is_eval:
                    print("reached_door.", end=' ')
                self.visited_left_door = True

        if self.reward_value is not None:
            self.row = row
            self.col = col


class GridWorldUnguidedAgent(GridWorldAgent):
    def __init__(self,
                 desc: List[str],
                 learner_cfg: GridWorldLearnerCfg):
        super(GridWorldUnguidedAgent, self).__init__(desc, learner_cfg)
        self.visited_left_door = False  # only used inside observe() to decide reward value

    def eval(self):
        super(GridWorldUnguidedAgent, self).eval()
        self.visited_left_door = False

    def observe(self, row: int, col: int, last_row: int, last_col: int):
        pos = [row / self.num_row, col / self.num_col]
        surroundings = []
        for r in [row - 1, row, row + 1]:
            for c in [col - 1, col, col + 1]:
                if r != row or c != col:
                    surroundings.append(float(self.desc[r][c] == 'B'))
        # observation size is 10
        self.observation = np.array(pos + surroundings)

        if self.row is None:
            self.row = row
        if self.col is None:
            self.col = col

        self.reward_value = None

        if self.step > 24:
            if self.is_eval:
                print("timeout!", end=' ')
            if np.random.rand() < 0.8:
                self.reward_value = 0.01 * (np.abs(row - self.row) + np.abs(col - self.col))
            else:
                if self.reward_state & GridWorldAgentRewardState.TIMED_OUT:
                    self.reward_value = 0.0
                else:
                    self.reward_value = -1.0
            self.prev_reward_state = self.reward_state
            self.reward_state |= GridWorldAgentRewardState.TIMED_OUT

        elif self.reward_state & GridWorldAgentRewardState.GET_FOOD:
            if self.desc[row][col] == 'F':
                if self.visited_left_door:
                    self.reward_value = 0.8
                else:
                    self.reward_value = 1.0
                if self.is_eval:
                    print("yum!", end=' ')
                self.prev_reward_state = self.reward_state
                self.reward_state ^= GridWorldAgentRewardState.GET_FOOD

                if self.reward_state & GridWorldAgentRewardState.TIMED_OUT:
                    self.reward_state ^= GridWorldAgentRewardState.TIMED_OUT

                self.visited_left_door = False

        elif not (self.reward_state & GridWorldAgentRewardState.GET_FOOD):
            if self.desc[row][col] == 'H':
                if self.visited_left_door:
                    self.reward_value = 0.8
                else:
                    self.reward_value = 1.0
                if self.is_eval:
                    print("zzz.", end=' ')
                self.prev_reward_state = self.reward_state
                self.reward_state |= GridWorldAgentRewardState.GET_FOOD

                if self.reward_state & GridWorldAgentRewardState.TIMED_OUT:
                    self.reward_state ^= GridWorldAgentRewardState.TIMED_OUT

                self.visited_left_door = False

        if not self.visited_left_door:
            if row == 5 and col == 3:
                if self.is_eval:
                    print("reached_door.", end=' ')
                self.visited_left_door = True

        if self.reward_value is not None:
            self.row = row
            self.col = col
