#!python3
from typing import Type, Optional
import numpy as np

from policy import PolicyPool
from learner import Learner
from gridworld import GridWorldAgentRewardState, GridWorldPolicyType, GridWorldLearnerCfg
from gridworld_policy import GridWorldBiasedPolicy, GridWorldUnbiasedPolicy


class GridWorldLearner(Learner):
    def __init__(self,
                 cfg: GridWorldLearnerCfg):
        super(GridWorldLearner, self).__init__()

        self.is_eval = False

        self.policy_type = cfg.policy_type
        self.population_size = cfg.population_size
        self.mutation_prob = cfg.mutation_prob
        self.refresh_prob = cfg.refresh_prob
        self.direction_change_prob = cfg.direction_change_prob

        # Dictionary of the population of policies.
        # Holds a population of policies (value) for each reward state (key).
        # The policy pool will be initialized once a new reward state is
        # encountered.
        self.policies = dict()
        # Mutated policies that are yet to be evaluated.
        self.tentative_policies = dict()
        # Holds the reference to the score-policy pair that
        # is to be re-evaluated.
        self.score_and_policy = None

        self.current_reward_state = None
        self.current_policy = None

    def eval(self):
        self.is_eval = True
        self.tentative_policies = dict()
        self.score_and_policy = None
        self.current_reward_state = None
        self.current_policy = None

    def train(self):
        self.is_eval = False

    def generate_policy(self):
        if self.policy_type == GridWorldPolicyType.UNBIASED:
            return GridWorldUnbiasedPolicy()
        elif self.policy_type == GridWorldPolicyType.BIASED:
            return GridWorldBiasedPolicy(
                direction_change_prob=self.direction_change_prob)
        else:
            raise ValueError("Invalid policy type {}.".format(
                self.policy_type))

    def decide(self, observation: np.ndarray,
               reward_state: GridWorldAgentRewardState, reward_value: int):
        # Initialize the policy pool for the reward state,
        # if that has not been done yet.
        if reward_state not in self.policies.keys():
            self.policies[reward_state] = PolicyPool(maxsize=self.population_size)

        if reward_state not in self.tentative_policies.keys():
            self.tentative_policies[reward_state] = []

        # If a reward is received
        if reward_value is not None:
            score = reward_value

            if self.score_and_policy is not None:
                # if not self.is_eval:
                #     print((self.current_reward_state.value, self.score_and_policy[0], score), end=' ')
                self.score_and_policy[0] = score
                self.score_and_policy[1] = self.current_policy.copy()
                self.policies[self.current_reward_state].heapify()
                self.score_and_policy = None
            else:
                self.policies[self.current_reward_state].put(
                    self.current_policy.copy(), score)

            self.current_policy = None

        if self.current_policy is None:
            self.current_reward_state = reward_state

            # If the policy pool is not full yet,
            # generate a random policy and evaluate it
            # Otherwise sample from the current population and mutate.
            # The sampled policy is also re-evaluated.
            if not self.policies[self.current_reward_state].full():
                self.current_policy = self.generate_policy()
            elif not self.tentative_policies[self.current_reward_state]:
                self.score_and_policy = self.policies[
                    self.current_reward_state].sample(temperature=1.5)
                score, policy = self.score_and_policy

                # Re-evaluate the sampled policy
                self.current_policy = policy.copy()

                if not self.is_eval:
                    # Sample another policy to mate with the current policy
                    # then mutate. The child policy is added to the tentative policy
                    # list and evaluated the next time the agent is in the same reward
                    # state.
                    tentative_policy = policy.copy()

                    # _, spouse_policy = self.policies[
                    #     self.current_reward_state].sample(temperature=5.0)
                    # tentative_policy = tentative_policy.mate(spouse_policy)
                    tentative_policy.mutate(self.mutation_prob)
                    self.tentative_policies[self.current_reward_state].append(
                        tentative_policy)

                    # a new policy as tentative policy
                    if np.random.rand() < self.refresh_prob:
                        tentative_policy = self.generate_policy()
                    self.tentative_policies[self.current_reward_state].append(
                        tentative_policy)


            else:
                self.current_policy = self.tentative_policies[
                    self.current_reward_state].pop()

        return self.current_policy.predict(observation)
