#!python3
from typing import Optional, Dict
import numpy as np

from copy import deepcopy
from policy import Policy
from gridworld import GridWorldAction


class GridWorldPolicy(Policy):
    def __init__(self,
                 policy_dict: Optional[Dict[bytes, GridWorldAction]] = None):
        super(GridWorldPolicy, self).__init__()

        if policy_dict is not None:
            self.policy_dict = policy_dict.copy()
        else:
            self.policy_dict = dict()

    def mutate(self, mutation_prob: float):
        key_pop_list = []  # the list of keys to be popped
        for key in self.policy_dict.keys():
            if np.random.rand() < mutation_prob:
                key_pop_list.append(key)
        for key in key_pop_list:
            self.policy_dict.pop(key)

    def mate(self, other):
        common_keys = set(self.policy_dict.keys()) & set(
            other.policy_dict.keys())
        self_keys = set(self.policy_dict.keys()) - set(
            other.policy_dict.keys())
        other_keys = set(other.policy_dict.keys()) - set(
            self.policy_dict.keys())

        child_policy_dict = dict()
        for key in self_keys:
            child_policy_dict[key] = self.policy_dict[key]
        for key in other_keys:
            child_policy_dict[key] = other.policy_dict[key]

        if np.random.rand() < 0.5:
            for key in common_keys:
                child_policy_dict[key] = self.policy_dict[key]
        else:
            for key in common_keys:
                child_policy_dict[key] = self.policy_dict[key]

        child = type(self).__new__(self.__class__)
        child.__init__(policy_dict=child_policy_dict)
        return child

    def predict(self, observation: np.ndarray):
        obs = observation.tobytes()
        if obs not in self.policy_dict.keys():
            self.policy_dict[obs] = self.generate_action()
        return self.policy_dict[obs]

    def copy(self):
        obj = type(self).__new__(self.__class__)
        obj.__init__(policy_dict=self.policy_dict.copy())
        return obj

    def generate_action(self, *arg, **kwargs):
        raise NotImplementedError


class GridWorldUnbiasedPolicy(GridWorldPolicy):
    def generate_action(self):
        return np.random.choice(GridWorldAction)


class GridWorldBiasedPolicy(GridWorldPolicy):
    directions = {
        'North': [0.76, 0.08, 0.08, 0.08],
        'NorthNorthEast': [0.56, 0.28, 0.08, 0.08],
        'NorthEastEast': [0.28, 0.56, 0.08, 0.08],
        'East': [0.08, 0.76, 0.08, 0.08],
        'SouthEastEast': [0.08, 0.56, 0.28, 0.08],
        'SouthSouthEast': [0.08, 0.28, 0.56, 0.08],
        'South': [0.08, 0.08, 0.76, 0.08],
        'SouthSouthWest': [0.08, 0.08, 0.56, 0.28],
        'SouthWestWest': [0.08, 0.08, 0.28, 0.56],
        'West': [0.08, 0.08, 0.08, 0.76],
        'NorthWestWest': [0.28, 0.08, 0.08, 0.56],
        'NorthNorthWest': [0.56, 0.08, 0.08, 0.28],
        "Here": [0.25, 0.25, 0.25, 0.25],  # remains the same place
    }

    def __init__(self,
                 direction_change_prob: Optional[float] = 0.0,
                 policy_dict: Optional[Dict[bytes, GridWorldAction]] = None,
                 direction: Optional[str] = None):
        super(GridWorldBiasedPolicy, self).__init__(policy_dict)

        self.direction_change_prob = direction_change_prob
        if direction is None:
            self.direction = np.random.choice(list(self.directions.keys()))
        elif direction in self.directions:
            self.direction = direction
        else:
            raise ValueError("Invalid direction {}".format(direction))

    def mate(self, other):
        child_policy_dict = super(GridWorldBiasedPolicy,
                                  self).mate(other).policy_dict

        if np.random.rand() < 0.5:
            direction_change_prob = self.direction_change_prob
            direction = self.direction
        else:
            direction_change_prob = other.direction_change_prob
            direction = other.direction

        child = GridWorldBiasedPolicy(
            direction_change_prob=direction_change_prob,
            policy_dict=child_policy_dict,
            direction=direction)
        return child

    def copy(self):
        obj = type(self).__new__(self.__class__)
        obj.__init__(direction_change_prob=self.direction_change_prob,
                     policy_dict=self.policy_dict.copy(),
                     direction=self.direction)
        return obj

    def generate_action(self):
        if np.random.rand() < self.direction_change_prob:
            self.direction = np.random.choice(list(self.directions.keys()))
        return np.random.choice(GridWorldAction,
                                p=self.directions[self.direction])
