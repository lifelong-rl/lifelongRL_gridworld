#!python3
import time
import numpy as np
import argparse
import pickle

from gridworld_env import GridWorld
from gridworld import GridWorldPolicyType, GridWorldAgentType, GridWorldLearnerCfg, GridWorldAgentRewardState

parser = argparse.ArgumentParser(description='Train a grid world lifelong learning agent.')
parser.add_argument('policy_type', metavar='POLICY_TYPE', type=str,
                    help='"biased" or "unbiased"')
parser.add_argument('agent_type', metavar='AGENT_TYPE', type=str,
                    help='"guided" or "unguided"')
parser.add_argument('num_trials', metavar='NUM_TRIALS', type=int, help='number of random trials')
args = parser.parse_args()

if args.policy_type == "biased":
    policy_type = GridWorldPolicyType.BIASED
elif args.policy_type == "unbiased":
    policy_type = GridWorldPolicyType.UNBIASED
else:
    raise ValueError("Invalid policy type {}.".format(args.policy_type))

if args.agent_type == "guided":
    agent_type = GridWorldAgentType.GUIDED
elif args.agent_type == "unguided":
    agent_type = GridWorldAgentType.UNGUIDED
else:
    raise ValueError("Invalid agent type {}.".format(args.agent_type))

file_name = '{}_{}_{}trials.pickle'.format(args.policy_type,
                                           args.agent_type,
                                           args.num_trials)
file = open(file_name, 'wb')

learner_cfg = GridWorldLearnerCfg(policy_type=policy_type,
                                  population_size=100,
                                  mutation_prob=0.05,
                                  refresh_prob=0.1,
                                  direction_change_prob=0.2)

get_food_reward_list = dict()

for n in range(args.num_trials):
    world = GridWorld(agent_type=agent_type, learner_cfg=learner_cfg)
    for i in range(10**8):
        if i != 0 and i % 500000 == 0:
            print("step (0.5 million): ", i / 500000)
            world.eval()
            for _ in range(10000):
                world.step()
            reward_history_GET_FOOD = world.agents[0][0].reward_history_GET_FOOD
            print(reward_history_GET_FOOD)
            for reward_state, next_reward_state in reward_history_GET_FOOD.items():
                if reward_state not in get_food_reward_list.keys():
                    get_food_reward_list[reward_state] = []
                if len(get_food_reward_list[reward_state]) <= n:
                    get_food_reward_list[reward_state].append([])
                get_food_reward_list[reward_state][n].append(next_reward_state)
            world.train()
        world.step()

print(get_food_reward_list)
pickle.dump(get_food_reward_list, file)

"""
print("**************")
print("**************")
print(world.agents[0][0].learner.policies)
print("**************")
world.render = True
world.eval()
for i in range(100):
    world.step()

    #time.sleep(0.1)
"""