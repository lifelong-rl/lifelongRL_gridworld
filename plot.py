import pickle
import numpy as np
import matplotlib.pyplot as plt
from gridworld import GridWorldAgentRewardState as Rs

filename = "unbiased_guided_20trials"
file = open(filename + ".pickle", "rb")
d = pickle.load(file)

#"""
reward_list = [~(Rs.GET_FOOD|~Rs.GET_FOOD),
               Rs.GET_FOOD|Rs.TIMED_OUT,
               Rs.GET_FOOD|Rs.REACHED_DOOR|Rs.TIMED_OUT]
legend_list = [r'$\neg \mathtt{GET\_FOOD} \wedge \neg \mathtt{TIMED\_OUT} \wedge \neg \mathtt{VISITED\_LEFT}$',
               r'$\mathtt{GET\_FOOD} \wedge \mathtt{TIMED\_OUT} \wedge \neg \mathtt{VISITED\_LEFT}$',
               r'$\mathtt{GET\_FOOD} \wedge \mathtt{TIMED\_OUT} \wedge \mathtt{VISITED\_LEFT}$']
#"""
"""
reward_list = [~(Rs.GET_FOOD|~Rs.GET_FOOD),
               Rs.GET_FOOD|Rs.TIMED_OUT]
legend_list = [r'$\neg \mathtt{GET\_FOOD} \wedge \neg \mathtt{TIMED\_OUT}$',
               r'$\mathtt{GET\_FOOD} \wedge \mathtt{TIMED\_OUT}$']
"""

arr_list = []
for rs in reward_list:
    arr_list.append(np.array(d[rs], dtype=np.double))
arr = np.stack(arr_list)
arr = arr / arr.sum(axis=0)  # normalize along the reward state

x = range(199)
y_list = [arr[i].mean(axis=0) for i, _ in enumerate(reward_list)]
for y in y_list:
    plt.plot(x, y)

plt.legend(legend_list)
plt.savefig(filename + ".png")
