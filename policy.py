#!python3
from typing import Optional
import heapq
import numpy as np
import multiprocessing as mp


class PolicyPool:
    """Policy pool that holds the population of policies.
    Essentially a customized priority queue.
    """
    def __init__(self, maxsize: int = 0, seed: Optional[int] = None):
        self.maxsize = maxsize
        self.np_random = np.random.RandomState(seed)
        self.lock = mp.Lock()

        with self.lock:
            self._q = []
            self._qsize = 0

    def qsize(self):
        with self.lock:
            return self._qsize

    def full(self):
        with self.lock:
            return self._qsize >= self.maxsize

    def put(self, item, priority):
        """Put an item into the queue. If the queue is full and the item
        has higher or equal priority than any item in the queue,
        replace the lowest-priority item and returns it. Otherwise return None.
        """
        with self.lock:
            popped_item = None
            if self._qsize >= self.maxsize:
                popped_priority, popped_item = heapq.heappop(self._q)
                self._qsize -= 1
                if popped_priority > priority:
                    priority = popped_priority
                    item = popped_item
                    popped_item = None
            heapq.heappush(self._q, [priority, item])
            self._qsize += 1
        return popped_item

    def sample(self, temperature: float):
        with self.lock:
            priority = np.array([x[0] for x in self._q])
            prob = np.exp(temperature*priority)
            prob = prob / prob.sum()
            sample_index = self.np_random.choice(range(len(self._q)), p=prob)
            sample = self._q[sample_index]
        return sample

    def heapify(self):
        with self.lock:
            heapq.heapify(self._q)

class Policy:
    def __init__(self, *args, **kwargs):
        pass

    def mutate(self, *args, **kwargs):
        raise NotImplementedError

    def predict(self, *args, **kwargs):
        raise NotImplementedError

    def copy(self, *args, **kwargs):
        raise NotImplementedError

    def __lt__(self, other):
        """For priority queue"""
        return False
