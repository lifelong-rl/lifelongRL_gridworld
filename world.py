#!python3
class World:
    def __init__(self, *args, **kwargs):
        pass

    def reset(self, *args, **kwargs):
        raise NotImplementedError

    def step(self, *args, **kwargs):
        raise NotImplementedError

    def close(self, *args, **kwargs):
        raise NotImplementedError
